# Pathtracer

A pathtracer software from the ["Ray Tracing in One Weekend"](https://raytracing.github.io/books/RayTracingInOneWeekend.html) book.

The codes are originally written in 2016 by Peter Shirley.